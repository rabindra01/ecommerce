package com.wawa.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.wawa.product.dto.OrderDetailsDto;
import com.wawa.product.service.OrderDetailsService;

@RestController
@RequestMapping("/orders")
public class OrderDetailsController {
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@GetMapping("/report")
	public  ResponseEntity<List<OrderDetailsDto>> getOrderDetailsByUser(@RequestParam Integer userid){
		
		List<OrderDetailsDto> listOrderDetailsDto=orderDetailsService.getOrderDetailsByUser(userid);
		
		return new ResponseEntity<>(listOrderDetailsDto,HttpStatus.OK);
	}

}
