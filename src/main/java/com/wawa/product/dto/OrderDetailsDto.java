package com.wawa.product.dto;


public class OrderDetailsDto {
	
	private Integer userid;
	private Integer pid;
	private String productName;
	private Integer quantity;
	private double price;

	
	public OrderDetailsDto() {
		super();
	}

	public OrderDetailsDto(Integer userid, Integer pid, String productName, Integer quantity, double price) {
		super();
		this.userid = userid;
		this.pid = pid;
		this.productName = productName;
		this.quantity = quantity;
		this.price = price;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
