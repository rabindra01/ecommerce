package com.wawa.product.dto;


public class ProductDto {
	
	private Integer pId;
	
	private String pName;

	private String category;
	
	private double price;
	
	public ProductDto() {
		super();
	}

	public ProductDto(Integer pId, String pName, String category, double price) {
		super();
		this.pId = pId;
		this.pName = pName;
		this.category = category;
		this.price = price;
	}

	public Integer getpId() {
		return pId;
	}

	public void setpId(Integer pId) {
		this.pId = pId;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
