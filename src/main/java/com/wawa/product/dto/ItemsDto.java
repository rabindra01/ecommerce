package com.wawa.product.dto;

public class ItemsDto {
	
	private Integer producId; 
	private Integer quantity;
	public Integer getProducId() {
		return producId;
	}
	public void setProducId(Integer producId) {
		this.producId = producId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
   
	
}
