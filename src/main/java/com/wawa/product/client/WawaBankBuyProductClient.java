package com.wawa.product.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="http://bank-service/bank")
public interface WawaBankBuyProductClient {
	
	@PostMapping("/fundtrans/transfer")
	public ResponseEntity<String> fundTransfer(@RequestParam Long sourceAccount, @RequestParam  Long destAccount, @RequestParam double amt );

}
