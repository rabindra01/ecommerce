package com.wawa.product.service.Impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wawa.product.client.WawaBankBuyProductClient;
import com.wawa.product.dto.FinalProductBuyDto;
import com.wawa.product.dto.ItemsDto;
import com.wawa.product.dto.ProductDto;
import com.wawa.product.entity.OrderDetails;
import com.wawa.product.entity.Product;
import com.wawa.product.entity.UserInfo;
import com.wawa.product.repository.OrderDetailsRepository;
import com.wawa.product.repository.SearchProductRepository;
import com.wawa.product.repository.UserInfoRepository;
import com.wawa.product.service.SearchProductService;

@Service
public class SearchProductServiceImpl implements SearchProductService {
	
	
    @Autowired
    SearchProductRepository searchProductRepository;
    
    @Autowired
	OrderDetailsRepository orderDetailsRepository;
    
    @Autowired
    UserInfoRepository userInfoRepository;
    
    @Autowired
    WawaBankBuyProductClient wawaBankBuyProductClient;
    
    private Integer destUserId=100;
    
	@Override
	public List<ProductDto> getProductByNameByCategory(String searchVar) {
		
		List<Product> listProduct=searchProductRepository.findByPnameLikeOrCategoryLike( searchVar, searchVar);
		 List<ProductDto> listProductDto=listProduct.stream().map(pdto->new ProductDto(pdto.getPid(),pdto.getPname(),pdto.getCategory(),
				 pdto.getPrice())).collect(Collectors.toList());
	
		return listProductDto;
	}

	@Override
	public String buyProducts(FinalProductBuyDto finalProdBuyDto) {
	String retValue = "No Buy Product";
	    
			Integer sourceUserId= finalProdBuyDto.getUserId();			
			List<ItemsDto> itemsDtos= finalProdBuyDto.getItemsDtos();
			double balance=0;
			Integer productId = null;
			Optional<Product> product=null;
			Product productF=null;
			  for(ItemsDto items :itemsDtos) { 
			      productId=items.getProducId();
			      Integer quanttity=items.getQuantity();
			      product = searchProductRepository.findById(productId);
			      if( product.isPresent()) {
			      productF=product.get();
			     double price= productF.getPrice();
			     balance=balance+price*quanttity;
			      }
			   }
			  
		    	UserInfo sourceUserInfo= userInfoRepository.findByUserid(sourceUserId);
			    UserInfo destUserInfo= userInfoRepository.findByUserid(destUserId);		 
		       ResponseEntity<String>  responseStr  =  wawaBankBuyProductClient.fundTransfer(sourceUserInfo.getAccountno(), destUserInfo.getAccountno(), balance); 
		       retValue = "Order happened";
		       if(responseStr.getStatusCode() == HttpStatus.OK) {
				 for(ItemsDto items :itemsDtos) { 
					  product = searchProductRepository.findById(items.getProducId());
					  if(product.isPresent()) {
						  OrderDetails orderDetails= new OrderDetails();
						  orderDetails.setPid(productId);
						  orderDetails.setProductName(product.get().getPname());
						  orderDetails.setQuantity(items.getQuantity());
						  orderDetails.setPrice(product.get().getPrice());
						  orderDetails.setUserId(sourceUserId);
						  orderDetailsRepository.save(orderDetails);
					  }
				  }
		       }
				return retValue;	
        }
}
