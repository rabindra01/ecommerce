package com.wawa.product.service;

import java.util.List;

import com.wawa.product.dto.FinalProductBuyDto;
import com.wawa.product.dto.ProductDto;



public interface SearchProductService {
	
    public List<ProductDto> getProductByNameByCategory(String searchVar);

	public String buyProducts(FinalProductBuyDto finalProdBuyDto);

}
