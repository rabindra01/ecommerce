package com.wawa.product.service.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wawa.product.dto.OrderDetailsDto;
import com.wawa.product.entity.OrderDetails;
import com.wawa.product.repository.OrderDetailsRepository;
import com.wawa.product.service.OrderDetailsService;

@Service
public class OrderDetailsServiceImpl implements OrderDetailsService {
	
	
	@Autowired
	OrderDetailsRepository orderDetailsRepository;
	
	@Override
	public List<OrderDetailsDto> getOrderDetailsByUser(Integer userid) {
		List<OrderDetails> listOrderDetails=orderDetailsRepository.findByUserId(userid);
		List<OrderDetailsDto> listOrderDetailsDto=listOrderDetails.stream().map(ord-> new OrderDetailsDto(ord.getUserId(), ord.getPid(),
				ord.getProductName(),ord.getQuantity(),ord.getPrice())).collect(Collectors.toList());
		return listOrderDetailsDto;
	}

}
