package com.wawa.product.service;

import java.util.List;

import com.wawa.product.dto.OrderDetailsDto;

public interface OrderDetailsService {

	List<OrderDetailsDto> getOrderDetailsByUser(Integer userid);
	

}
