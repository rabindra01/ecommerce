package com.wawa.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wawa.product.entity.Product;

@Repository
public interface SearchProductRepository extends JpaRepository<Product, Integer> {

	List<Product> findByPnameLike(String searchVar);

	List<Product> findByPnameLikeAndCategoryLike(String searchVar, String searchVar2);

	List<Product> findByPnameLikeOrCategoryLike(String searchVar, String searchVar2);

}
