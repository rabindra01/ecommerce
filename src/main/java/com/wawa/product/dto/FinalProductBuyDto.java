package com.wawa.product.dto;

import java.util.ArrayList;
import java.util.List;

public class FinalProductBuyDto {
	
	private Integer userId;
	
	private List<ItemsDto> ItemsDtos= new ArrayList<>();

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<ItemsDto> getItemsDtos() {
		return ItemsDtos;
	}

	public void setItemsDtos(List<ItemsDto> itemsDtos) {
		ItemsDtos = itemsDtos;
	}

	
   
}
