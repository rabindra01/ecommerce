package com.wawa.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wawa.product.dto.FinalProductBuyDto;
import com.wawa.product.dto.ProductDto;
import com.wawa.product.service.SearchProductService;


@RestController
@RequestMapping("/products")
public class SearchProductController {
	
	@Autowired
	SearchProductService searchProductService;
	
	
	@GetMapping("/search")
    public ResponseEntity<List<ProductDto>> getProductByNameByCategory(@RequestParam String searchVar){
		List<ProductDto> listproductDto=searchProductService.getProductByNameByCategory(searchVar);
		 return new ResponseEntity<>(listproductDto,HttpStatus.OK);
    }
	
	@PostMapping("/buy")
	public ResponseEntity<String> fundTransfer(@RequestBody FinalProductBuyDto finalProdBuyDto) {	
		  String strRes=searchProductService.buyProducts(finalProdBuyDto);	
		 return new ResponseEntity<>(strRes,HttpStatus.OK);
	}	  
	

}
